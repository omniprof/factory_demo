package com.kenfogel.factory_demo.isp.abstractfactory;

import com.kenfogel.factory_demo.isp.business.ISPBusinessImpl;
import com.kenfogel.factory_demo.isp.data.ISPBean;
import com.kenfogel.factory_demo.isp.presentation.ISPPresentation;

/**
 * Interface for factory
 * @author Ken
 */
public interface AbstractPresentationFactory {
    public ISPPresentation createPresentation(ISPBean ispBean, ISPBusinessImpl ispBusiness);
}
