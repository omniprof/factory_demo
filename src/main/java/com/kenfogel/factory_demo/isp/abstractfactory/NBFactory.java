package com.kenfogel.factory_demo.isp.abstractfactory;

import com.kenfogel.factory_demo.isp.business.ISPBusinessImpl;
import com.kenfogel.factory_demo.isp.data.ISPBean;
import com.kenfogel.factory_demo.isp.presentation.ISPGUINBPresentationImpl;
import com.kenfogel.factory_demo.isp.presentation.ISPPresentation;

/**
 * Create the NetBeans presentation object
 * @author Ken
 */
public class NBFactory implements AbstractPresentationFactory{

    @Override
    public ISPPresentation createPresentation(ISPBean ispBean, ISPBusinessImpl ispBusiness) {
        ISPPresentation isp = new ISPGUINBPresentationImpl(ispBean,ispBusiness);
        return isp;
    }
    
}

