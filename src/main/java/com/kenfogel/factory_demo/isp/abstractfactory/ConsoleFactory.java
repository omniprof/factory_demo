package com.kenfogel.factory_demo.isp.abstractfactory;

import com.kenfogel.factory_demo.isp.business.ISPBusinessImpl;
import com.kenfogel.factory_demo.isp.data.ISPBean;
import com.kenfogel.factory_demo.isp.presentation.ISPConsolePresentationImpl;
import com.kenfogel.factory_demo.isp.presentation.ISPPresentation;

/**
 * Create the console presentation object
 * @author Ken
 */
public class ConsoleFactory implements AbstractPresentationFactory{

    @Override
    public ISPPresentation createPresentation(ISPBean ispBean, ISPBusinessImpl ispBusiness) {
        ISPPresentation isp = new ISPConsolePresentationImpl(ispBean,ispBusiness);
        return isp;
    }
    
}
