package com.kenfogel.factory_demo.isp.abstractfactory;

/**
 * Return the appropriate factory
 * @author Ken
 */
public class GUIBuilder {
    
    public static AbstractPresentationFactory builder(String selection) {
        AbstractPresentationFactory apf = null;
        switch (selection) {
            case "Console":
                apf = new ConsoleFactory();
                break;
            case "NB":
                apf = new NBFactory();
                break;
            case "Eclipse":
                apf = new EclipseFactory();
                break;
        }
        return apf;
    }
}
