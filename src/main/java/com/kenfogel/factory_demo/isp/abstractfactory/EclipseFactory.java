package com.kenfogel.factory_demo.isp.abstractfactory;

import com.kenfogel.factory_demo.isp.business.ISPBusinessImpl;
import com.kenfogel.factory_demo.isp.data.ISPBean;
import com.kenfogel.factory_demo.isp.presentation.ISPGUIPresentationImpl;
import com.kenfogel.factory_demo.isp.presentation.ISPPresentation;

/**
 * Create the Eclipse presentation object
 * @author Ken
 */
public class EclipseFactory implements AbstractPresentationFactory{

    @Override
    public ISPPresentation createPresentation(ISPBean ispBean, ISPBusinessImpl ispBusiness) {
        ISPPresentation isp = new ISPGUIPresentationImpl(ispBean,ispBusiness);
        return isp;
    }
    
}

