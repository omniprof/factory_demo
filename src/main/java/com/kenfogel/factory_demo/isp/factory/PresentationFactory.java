package com.kenfogel.factory_demo.isp.factory;

import com.kenfogel.factory_demo.isp.business.ISPBusinessImpl;
import com.kenfogel.factory_demo.isp.data.ISPBean;
import com.kenfogel.factory_demo.isp.presentation.ISPConsolePresentationImpl;
import com.kenfogel.factory_demo.isp.presentation.ISPGUINBPresentationImpl;
import com.kenfogel.factory_demo.isp.presentation.ISPGUIPresentationImpl;
import com.kenfogel.factory_demo.isp.presentation.ISPPresentation;

/**
 * Factory Method
 * 
 * @author Ken
 */
public class PresentationFactory {
    
    public static ISPPresentation createPresentation(String selection, ISPBean ispBean, ISPBusinessImpl ispBusiness) {
        switch (selection) {
            case "Console":
                return new ISPConsolePresentationImpl(ispBean, ispBusiness);
            case "NB":
                return new ISPGUINBPresentationImpl(ispBean, ispBusiness);
            case "Eclipse":
                return new ISPGUIPresentationImpl(ispBean, ispBusiness);
            default:
                throw new IllegalArgumentException("Selection does not exit");
        }
    }
}
