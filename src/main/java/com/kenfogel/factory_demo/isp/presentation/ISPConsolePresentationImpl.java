package com.kenfogel.factory_demo.isp.presentation;

import com.kenfogel.factory_demo.isp.business.ISPBusiness;
import java.text.NumberFormat;
import java.util.Scanner;

import com.kenfogel.factory_demo.isp.data.ISPBean;

/**
 * This class is responsible for the presentation layer of the program. It can
 * easily be replaced by a GUI class without any effect on the ISPbean or
 * ISPBusinessImpl classes
 *
 * @author Ken Fogel
 * @version 1.0
 */
public class ISPConsolePresentationImpl implements ISPPresentation {

    private final Scanner sc;
    private final ISPBean ispBean;
    private final ISPBusiness ispBusiness;

    /**
     * Non-default Constructor
     *
     * @param ispBean
     * @param ispBusiness
     */
    public ISPConsolePresentationImpl(ISPBean ispBean, ISPBusiness ispBusiness) {
        super();
        this.ispBean = ispBean;
        this.ispBusiness = ispBusiness;
        sc = new Scanner(System.in, "UTF-8");

    }

    /**
     * Request from the user the letter of the plan they are using
     */
    private void requestPlan() {
        System.out.println("Select an Internet Service plan (A/B/C)");

        // Primary Read
        char plan;
        if (sc.hasNext("[a-cA-C]")) { // Regular expression that only accepts a
            // single letter
            plan = sc.next().toUpperCase().charAt(0);
        } else {
            plan = 'z';
            System.out.println("Invalid plan type, please try again");
        }
        sc.nextLine();

        // Secondary Read
        while (plan > 'c') {
            if (sc.hasNext("[a-cA-C]")) {
                plan = sc.next().toUpperCase().charAt(0);
            } else {
                System.out.println("Invalid plan type, please try again");
                plan = 'z';
            }
            sc.nextLine();
        }
        ispBean.setPlanType(plan);
    }

    /**
     * Request the total number of hours, between 0 - 744 inclusive, that are
     * used
     */
    private void requestHours() {

        int hours;

        // Primary Read
        System.out.println("Enter the number of hours you used for the month.");
        if (sc.hasNextInt()) {
            hours = sc.nextInt();
        } else {
            hours = -1;
        }
        sc.nextLine();

        // Secondary Read
        while (hours < 0 || hours > 744) {
            System.out.println("Sorry, your input was invalid");
            System.out
                    .println("Enter the number of hours you used for the month.");
            if (sc.hasNextInt()) {
                hours = sc.nextInt();
            } else {
                hours = -1;
            }
            sc.nextLine();
        }
        ispBean.setTotalHours(hours);
    }

    /**
     * Display the billing report
     */
    private void displayResults() {

        NumberFormat cf = NumberFormat.getCurrencyInstance();

        ispBusiness.updateISPBean();

        System.out.println("                    Plan Type: "
                + ispBean.getPlanType());
        System.out.println("       Included Hours in Plan: "
                + ispBean.getIncludedHours());
        System.out.println("             Additional Hours: "
                + ispBusiness.getAdditionalHours());
        System.out.println("                  Total Hours: "
                + ispBean.getTotalHours());
        System.out.println();
        System.out.println("                    Plan Rate: "
                + cf.format(ispBean.getPlanCost()));
        System.out.println("       Additional Hourly Rate: "
                + cf.format(ispBean.getHourlyCost()));
        System.out.println("            Total Hourly Cost: "
                + cf.format(ispBusiness.getAdditionalHoursCharge()));
        System.out.println();
        System.out.println("                   Total Cost: "
                + cf.format(ispBusiness.getTotalCost()));
    }

    /*
     * (non-Javadoc)
     * 
     * @see com.kenfogel.isp.presentation.ISPPresentation#perform() The
     * operations in this console interface must occur in a specific order
     */
    @Override
    public void perform() {
        requestPlan();
        requestHours();
        displayResults();
    }
}
