package com.kenfogel.factory_demo.isp;

import com.kenfogel.factory_demo.isp.abstractfactory.AbstractPresentationFactory;
import com.kenfogel.factory_demo.isp.abstractfactory.GUIBuilder;

import com.kenfogel.factory_demo.isp.business.ISPBusinessImpl;
import com.kenfogel.factory_demo.isp.data.ISPBean;
import com.kenfogel.factory_demo.isp.factory.PresentationFactory;
import com.kenfogel.factory_demo.isp.presentation.ISPPresentation;

/**
 * The app class for a sample program that demonstrates a layered architecture
 *
 * @author Ken Fogel
 * @version 1.0
 *
 */
public class ISPFactoryApp {

    private ISPBusinessImpl ispBusiness;
    private ISPBean ispBean;
    private ISPPresentation ispPresentation;

    /**
     * Constructor instantiates the required classes and transfers control
     */
    public ISPFactoryApp() {
        super();

    }

    public void performFactory() {
        ispBean = new ISPBean();
        ispBusiness = new ISPBusinessImpl(ispBean);

        // use the Factory, Console, NB or Eclipse
        ispPresentation = PresentationFactory.createPresentation("Console", ispBean, ispBusiness);

        ispPresentation.perform();
    }

    public void performAbstractFactory() {
        ispBean = new ISPBean();
        ispBusiness = new ISPBusinessImpl(ispBean);

        // use the Abstract Factory to select the object that will create a Console, NB or Eclipse GUI
        AbstractPresentationFactory asp = GUIBuilder.builder("NB");

        ispPresentation = asp.createPresentation(ispBean, ispBusiness);

        ispPresentation.perform();
    }

    /**
     * Start the app in event queue thread to guarantee proper operation of the
     * Swing GUI.
     *
     * @param args
     */
    public static void main(String[] args) {
        ISPFactoryApp isp = new ISPFactoryApp();
        isp.performFactory();
        //isp.performAbstractFactory();
        System.exit(0);
    }
}
